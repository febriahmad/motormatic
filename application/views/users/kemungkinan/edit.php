<div class="content-wrapper">
	<div class="content">
		<?php
			echo $this->session->flashdata('msg');
		?>
		<div class="row">
			<div class="col-md-12">
				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
					<div class="panel panel-flat">
						<div class="panel-body">
							<fieldset class="content-group">
								<legend class="text-bold"><i class="icon-clipboard6"></i> <?php echo $judul_web; ?></legend>
								<div class="form-group">
									<label>Kode Gejala Inti</label>
									<select class="form-control" name="kode_gejala">
										<option value>Pilih Kode Gejala</option>
										<?php foreach ($list_query_gejala->result() as $item) { ?>
											<?php 
												$selected = "";
												if($item->kode_gejala == $obj->kode_gejala){
													$selected = "selected";
												}
											?>
											<option <?php echo $selected ?> value="<?php echo $item->kode_gejala ?>"><?php echo $item->kode_gejala ?> | <?php echo $item->nama_gejala ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label>Kode Gejala Saat Ini</label>
									<select class="form-control" name="kode_gejala_sebelumnya">
										<option value>Pilih Kode Gejala</option>
										<?php foreach ($list_query_gejala->result() as $item) { ?>
											<?php 
												$selected_prev = "";
												if($item->kode_gejala == $obj->kode_gejala_sebelumnya){
													$selected_prev = "selected";
												}
											?>
											<option <?php echo $selected_prev ?> value="<?php echo $item->kode_gejala ?>"><?php echo $item->kode_gejala ?> | <?php echo $item->nama_gejala ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label>Kode Gejala Selanjutnya</label>
									<select class="form-control" name="kode_gejala_selanjutnya">
										<option value>Pilih Kode Gejala</option>
										<?php foreach ($list_query_gejala->result() as $item) { ?>
											<?php 
												$selected_next = "";
												if($item->kode_gejala == $obj->kode_gejala_selanjutnya){
													$selected_next = "selected='selected'";
												}
											?>
											<option <?php echo $selected_next ?> value="<?php echo $item->kode_gejala ?>"><?php echo $item->kode_gejala ?> | <?php echo $item->nama_gejala ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label>Kode Penyakit</label>
									<select class="form-control" name="kode_penyakit">
										<option value>Pilih Kode Penyakit</option>
										<?php foreach ($list_query_penyakit->result() as $item) { ?>
											<?php 
												$selected_penyakit = "";
												if($item->kode_penyakit == $obj->kode_penyakit){
													$selected_penyakit = "selected";
												}
											?>
											<option <?php echo $selected_penyakit ?> value="<?php echo $item->kode_penyakit ?>"><?php echo $item->kode_penyakit ?> | <?php echo $item->nama_penyakit ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-check">
									<?php 
										$checked = "";
										if($obj->bercabang == "Ya"){
											$checked = "checked";
										}
									?>
									<input <?php echo $checked ?> type="checkbox" class="form-check-input" id="exampleCheck1" name="bercabang">
									<label class="form-check-label" for="exampleCheck1">Centang jika kode gejala bercabang</label>
								 </div>
							</fieldset>
						</div>
						<div class="panel-footer text-right">
							<button class="btn btn-primary" style="margin-right: 10px;"><i class="icon-floppy-disk"></i> Edit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>